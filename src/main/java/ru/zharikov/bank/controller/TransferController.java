package ru.zharikov.bank.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.zharikov.bank.model.Transfer;
import ru.zharikov.bank.service.TransferService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class TransferController {
    private final TransferService transferService;

    @GetMapping("/admin/transfer")
    public ModelAndView transfer() {
        ModelAndView modelAndView = new ModelAndView();
        Transfer transfer = new Transfer();
        modelAndView.addObject("transfer", transfer);
        modelAndView.setViewName("admin/transfer");
        return modelAndView;
    }

    @PostMapping("/admin/transfer")
    public ModelAndView transfer(@Valid Transfer transfer) {
        ModelAndView modelAndView = new ModelAndView();
        transferService.send(transfer);
        modelAndView.addObject("result", "Перевод отправлен");
        modelAndView.addObject("transfer", new Transfer());
        modelAndView.setViewName("admin/transfer");
        return modelAndView;
    }
}
