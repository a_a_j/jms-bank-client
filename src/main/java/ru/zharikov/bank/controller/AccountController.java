package ru.zharikov.bank.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.zharikov.bank.model.Account;
import ru.zharikov.bank.service.AccountService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/admin/open")
    public ModelAndView openAccount() {
        ModelAndView modelAndView = new ModelAndView();
        Account account = new Account();
        modelAndView.addObject("account", account);
        modelAndView.setViewName("admin/open");
        return modelAndView;
    }

    @PostMapping("/admin/open")
    public ModelAndView openAccount(@Valid Account account, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("admin/home");
        } else {
            Account newAccount = accountService.save(account);
            modelAndView.addObject("newAccount", "Счет "+newAccount.getNumber()+" успешно открыт");
            modelAndView.setViewName("admin/open");
        }
        return modelAndView;
    }

    @GetMapping("/admin/refill")
    public ModelAndView refillAccount() {
        ModelAndView modelAndView = new ModelAndView();
        Account account = new Account();
        modelAndView.addObject("account", account);
        modelAndView.setViewName("admin/refill");
        return modelAndView;
    }

    @PostMapping("/admin/refill")
    public ModelAndView refillAccount(@Valid Account account) {
        ModelAndView modelAndView = new ModelAndView();
        accountService.recharge(account);
        modelAndView.addObject("newAccount", "Счет "+account.getNumber()+" успешно пополнен");
        modelAndView.addObject("account", new Account());
        modelAndView.setViewName("admin/refill");
        return modelAndView;
    }

    @GetMapping("/admin/close")
    public ModelAndView closeAccount() {
        ModelAndView modelAndView = new ModelAndView();
        Account account = new Account();
        modelAndView.addObject("account", account);
        modelAndView.setViewName("admin/close");
        return modelAndView;
    }

    @PostMapping("/admin/close")
    public ModelAndView closeAccount(@Valid Account account) {
        ModelAndView modelAndView = new ModelAndView();
        accountService.close(account);
        modelAndView.addObject("success", "Счет "+account.getNumber()+" закрыт");
        modelAndView.addObject("account", new Account());
        modelAndView.setViewName("admin/close");
        return modelAndView;
    }
}
