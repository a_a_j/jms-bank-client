package ru.zharikov.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.zharikov.bank.model.Transfer;

@Repository("transferRepository")
@Deprecated
public interface TransferRepository extends JpaRepository<Transfer, Integer> {
    Transfer save(Transfer transfer);
}
