package ru.zharikov.bank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="transfer")
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transfer_id")
    private int id;

    @Column
    //@NotEmpty(message = "Поле не может быть пустым")
    private int sender;

    @Column
    //@NotEmpty(message = "Поле не может быть пустым")
    private int recipient;

    @Column
    //@NotEmpty(message = "Поле не может быть пустым")
    private double amount;

    @Column
    //@NotEmpty(message = "Поле не может быть пустым")
    private int status;
}
