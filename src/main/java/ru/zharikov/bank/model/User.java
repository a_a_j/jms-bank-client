package ru.zharikov.bank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int id;

    @Column
    @Email(message = "Пожалуйста проверьте Email")
    @NotEmpty(message = "Поле не может быть пустым")
    private String email;

    @Column
    @Length(min = 5, message = "Пароль должен быть минимум 5 символов")
    @NotEmpty(message = "Поле не может быть пустым")
    private String password;

    @Column
    @NotEmpty(message = "Поле не может быть пустым")
    private String name;

    @Column(name = "last_name")
    @NotEmpty(message = "Поле не может быть пустым")
    private String lastName;

    @Column
    private int active;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

}
