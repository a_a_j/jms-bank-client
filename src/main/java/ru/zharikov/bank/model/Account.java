package ru.zharikov.bank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private int id;

    @Column
    //@NotEmpty(message = "Поле не может быть пустым")
    private String firstName;

    @Column
    private String middleName;

    @Column(name = "last_name")
    //@NotEmpty(message = "Поле не может быть пустым")
    private String lastName;

    @Column(unique = true)
    private int number;

    @Column
    private double balance;

    @Column
    private int active;
}
