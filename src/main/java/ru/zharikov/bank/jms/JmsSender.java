package ru.zharikov.bank.jms;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.MapMessage;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class JmsSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(JmsSender.class);

    @Autowired
    private ConnectionFactory connectionFactory;
    private JmsTemplate jmsTemplate;

    @PostConstruct
    public void init() {
        this.jmsTemplate = new JmsTemplate(connectionFactory);
    }

    public void sendMessage(String queueName, String operation, String value,
                            UUID correlationId) {
        try {
            jmsTemplate.send(queueName, session -> {
                MapMessage mapMessage = session.createMapMessage();
                mapMessage.setString(operation, value);
                mapMessage.setJMSCorrelationID(correlationId.toString());
                return mapMessage;
            });
        } catch (JmsException e) {
            LOGGER.warn("Error on sending message", e);
        }
    }
}
