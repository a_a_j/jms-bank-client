package ru.zharikov.bank.dao;

import ru.zharikov.bank.model.Transfer;

public interface TransferDao {
    Transfer perform (String operation, String value);
}
