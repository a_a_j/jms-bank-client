package ru.zharikov.bank.dao;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.zharikov.bank.jms.JmsReceiver;
import ru.zharikov.bank.jms.JmsSender;
import ru.zharikov.bank.model.Transfer;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class TransferDaoJms implements TransferDao {
    private static final String TRANSFER_REQUEST = "transfer.in";
    private static final String TRANSFER_RESPONSE = "transfer.out";

    private final JmsReceiver receiver;
    private final JmsSender sender;
    private Gson gson = new Gson();

    @Override
    public Transfer perform(String operation, String value) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(TRANSFER_REQUEST, operation, value,
                correlationID);
        return gson.fromJson(
                receiver.receiveMessage(TRANSFER_RESPONSE, correlationID),
                Transfer.class);
    }
}
