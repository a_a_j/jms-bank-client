package ru.zharikov.bank.dao;

import ru.zharikov.bank.model.Account;

public interface AccountDao {
    Account perform (String operation, String value);
}
