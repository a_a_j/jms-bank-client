package ru.zharikov.bank.dao;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.zharikov.bank.jms.JmsReceiver;
import ru.zharikov.bank.jms.JmsSender;
import ru.zharikov.bank.model.Account;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AccountDaoJms implements AccountDao {
    private static final String ACCOUNT_REQUEST = "account.in";
    private static final String ACCOUNT_RESPONSE = "account.out";

    private final JmsReceiver receiver;
    private final JmsSender sender;
    private Gson gson = new Gson();

    @Override
    public Account perform(String operation, String value) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(ACCOUNT_REQUEST, operation, value,
                correlationID);
        return gson.fromJson(
                receiver.receiveMessage(ACCOUNT_RESPONSE, correlationID),
                Account.class);
    }
}
