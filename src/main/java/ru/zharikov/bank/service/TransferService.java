package ru.zharikov.bank.service;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zharikov.bank.dao.TransferDao;
import ru.zharikov.bank.model.Transfer;

@Service("transferService")
@RequiredArgsConstructor
public class TransferService {
    private final TransferDao transferRepository;
    private Gson gson = new Gson();

    public Transfer send(Transfer transfer) {
        return transferRepository.perform("save", gson.toJson(transfer));
    }
}
