package ru.zharikov.bank.service;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zharikov.bank.dao.AccountDao;
import ru.zharikov.bank.model.Account;

@Service("accountService")
@RequiredArgsConstructor
public class AccountService {
    private final AccountDao accountRepository;
    private Gson gson = new Gson();

    public Account findByNumber(int number){
        return accountRepository.perform("find", String.valueOf(number));
    }

    public Account save(Account account) {
        return accountRepository.perform("save", gson.toJson(account));
    }

    public void recharge(Account account) {
        accountRepository.perform("recharge", gson.toJson(account));
    }

    public void withdraw(Account account) {
        accountRepository.perform("withdraw", gson.toJson(account));
    }

    public void close(Account account) {
        accountRepository.perform("close", gson.toJson(account));
    }
}
